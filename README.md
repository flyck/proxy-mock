# Proxy Mock

A proxy server that can mock individual requests but proxy the rest.

Can be a helpful tool to confirm a number of HTTP codes are caught correctly when no frontend
tests are written, which is often the norm.

Can also help when replicating errors that only occur in certain edge-cases, or with a certain
endpoint.

## How to use

1) Point the embedded proxy at your api
2) Mock responses where needed
3) Start the server
```sh
npm run dev --port 1234
```
4) Point your frontend at the server (http://localhost:1234)

## TS vs JS

If you dont like the typescript and all the dependencies, there is a more lightweight version in
the `js` branch.

## Learn More

To learn Fastify, check out the [Fastify documentation](https://www.fastify.io/docs/latest/).
