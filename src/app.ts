import { fastifyHttpProxy } from '@fastify/http-proxy'
import { FastifyPluginAsync, FastifyReply } from 'fastify'
import { NotFound, Forbidden, GatewayTimeout, } from "http-errors"
import sensible from '@fastify/sensible'

const PROXY_URL = "https://pokeapi.co/api/v2/pokemon/"

const app: FastifyPluginAsync = async (
  fastify,
  opts
): Promise<void> => {
  fastify.get('/ditto', async function(request, reply) {
    return { description: "my own ditto!" }
  })

  fastify.get('/maracamba', async function(request, reply) {
    errorCodes(reply)
  })

  fastify.register(fastifyHttpProxy, {
    upstream: PROXY_URL,
    prefix: "/",
  })
  fastify.register(sensible)
};

let COUNTER = 3

// Returns different common error codes on each refresh. Useful to test if these errors are
// prorperly caught by the frontend
function errorCodes(reply: FastifyReply) {
  // TODO switch to httpErrors from the fastify sensible package
  switch (COUNTER) {
    case 0: // unauthenticated
      COUNTER = 1
      reply
        .code((new Forbidden).statusCode)
      break;
    case 1: // not found
      COUNTER = 2
      reply
        .code((new NotFound).statusCode)
      break;
    case 2: // timeout
      COUNTER = 3
      reply
        .code((new GatewayTimeout).statusCode)
      break;
    case 3: // empty
      COUNTER = 0
      reply
        .header("access-control-allow-origin", "*")
        .send()
      break;
  }
}

export default app;
export { app }
